FROM node 
#Используем node.js для сборки приложения

RUN mkdir /app
#Создаём директорию собранного приложения

WORKDIR /app
#Устанавливаем её в качестве рабочей директории

COPY package.json /app
#копируем конфиг, чтобы быстрее собиралось

COPY . /app
#копируем исходники в папку app

RUN yarn install
#Собираем пакеты для сборки приложения и формируем package.json

#RUN yarn upgrade
#И ещё апгрейды

RUN yarn build
#собираем это дерьмо

EXPOSE 3000
#открываем порт 

ENTRYPOINT yarn start
#запускаем